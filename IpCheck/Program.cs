﻿using System.Net.NetworkInformation;

namespace IpCheck
{
    class Program
    {
        private static readonly string _url = "log.txt";
        private static Ping _ping;

        public static void Main(string[] args)
        {
            _ping= new Ping();

            int maxNumber = 255;

            for(int iOne = 192; iOne <= maxNumber; iOne++)
            {
                for(int iTwo = 168; iTwo <= maxNumber; iTwo++)
                {
                    for(int iThree = 0; iThree <= maxNumber; iThree++)
                    {
                        Console.WriteLine($"iThree: {iThree}");

                        for (int iFour = 1; iFour <= maxNumber; iFour++)
                        {
                            string resultConcat = $"{iOne}.{iTwo}.{iThree}.{iFour}";
                            Ping(resultConcat).Wait();
                        }
                    }
                }
            }
        }

        private async static Task Ping(string ip)
        {
            int timeOut = 1;

            PingReply pingReply = await _ping.SendPingAsync(ip, timeOut);

            if (pingReply.Status == IPStatus.Success)
            {
                Console.WriteLine($"{pingReply.Status}, {pingReply.Address}");
                await AppendMessageInFile(pingReply.Address.ToString(),FileMode.Append);
            }
            else
            {
                Console.WriteLine(ip);
            }

        }

        private async static Task AppendMessageInFile(string message, FileMode fileMode)
        {
            if (!File.Exists(_url))
            {
                fileMode = FileMode.OpenOrCreate;
                message = "";
            }

            using (FileStream file = new FileStream(_url, fileMode))
            {
                using (StreamWriter stream = new StreamWriter(file))
                {
                    await stream.WriteLineAsync(message);
                }
            }
        }
    }
}